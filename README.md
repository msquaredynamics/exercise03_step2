# Exercise03_step2

Step 2 of the Exercise03. This version displays a (hardcoded, fixed) list of Articles, and allowes the user to click on a title.
When a title is selected:

- it starts a new activity that displays the title and description of the selected title if the screen is in PORTRAIT (vertical) mode
- it displays both the titles and the selected article's details in the same screen, using two fragments.

This version still use 2 Activities (MainActivity, DetailsActivity)

