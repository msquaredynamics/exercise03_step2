package com.msquaredynamics.exercise03

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_details.view.*



class DetailsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val mView = inflater.inflate(R.layout.fragment_details, container, false)

        val articleId = arguments?.getInt(ARG_ID, 0)

        articles.find {
            it.id == articleId
        }?.also {
            mView.textview_frag_details_title.text = it.title
            mView.textview_frag_details_description.text = it.description
        }

        return mView
    }


    /** Returns the currently shown article id or null if no article is shown */
    fun shownId() = arguments?.getInt(ARG_ID, -1)


    companion object {
        const val ARG_ID = "articleId"

        /**
         * Creates a new instance of the DetailsFragment, passing the id of the article to display as argument
         */
        @JvmStatic
        fun newInstance(id: Int) =
            DetailsFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_ID, id)
                }
            }
    }

}
