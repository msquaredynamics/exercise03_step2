package com.msquaredynamics.exercise03

import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class DetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            finish()
        }

        if (savedInstanceState == null) {
            val details = DetailsFragment().apply {
                arguments = intent.extras
            }

            /*
             * android.R.id.content is always the root view of the layout. It is a handy way to access the layout without
             * knowing its ID.
             */
            supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, details)
                .commit()
        }

        /**
         * setContentView is not called here because we don't need to inflate a layout for the activity. The layout
         * will be inflated directly by the DetailsFragment instance.
         */
    }


    /**
     * When user presses the back button, we reset the selected article
     */
    override fun onBackPressed() {
        selectedArticleId = -1
        super.onBackPressed()
    }

}
