package com.msquaredynamics.exercise03

import android.content.Intent
import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle


data class Article(var id: Int, var title: String, var description: String)

val articles = listOf(
    Article(0, "Title01", "Description of title 01"),
    Article(1, "Title02","Description of title 02"),
    Article(2, "Title03","Description of title 03")
)

var selectedArticleId : Int = -1

class MainActivity : AppCompatActivity(), TitlesFragment.OnArticleSelectedListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState != null) {
            selectedArticleId = savedInstanceState.getInt(KEY_SELECTED_ARTICLE, -1)
            showDetails()
        }
    }


    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putInt(KEY_SELECTED_ARTICLE, selectedArticleId)
        super.onSaveInstanceState(outState)
    }


    override fun onArticleSelected(id: Int) {
        selectedArticleId = id
        showDetails()
    }


    /**
     * Invoked to show details of a selected article.
     */
    private fun showDetails() {
        val id = selectedArticleId

        /*
        * When an article is selected, we need to check the screen orientation first.
        * If we are in portrait mode, we start the DetailsActivity to display the article's details
        *
        * If we are in landscape mode, we replace the fragment currently visibile (if needed) with a new fragment
        */
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            if (id != -1) {
                val mIntent = Intent(this, DetailsActivity::class.java).apply {
                    putExtra(DetailsFragment.ARG_ID, id)
                }

                startActivity(mIntent)
            }
        } else {
            // Landscape orientation
            var details = supportFragmentManager.findFragmentById(R.id.fragmentcontainer_activity_main) as? DetailsFragment

            if (details?.shownId() != id) {

                if (id == -1) { // No title selected. Just remove the visible fragment, if any

                    details?.let {
                        supportFragmentManager.beginTransaction().apply {
                            remove(details!!)
                            commit()
                        }
                    }
                } else {
                    // Current fragment displays another article. We need to replace it
                    details = DetailsFragment.newInstance(id)
                    supportFragmentManager.beginTransaction().apply {
                        replace(R.id.fragmentcontainer_activity_main, details)
                        commit()
                    }
                }
            }
        }
    }


    companion object {
        @JvmStatic
        private val KEY_SELECTED_ARTICLE = "selectedArticleId"
    }
}
