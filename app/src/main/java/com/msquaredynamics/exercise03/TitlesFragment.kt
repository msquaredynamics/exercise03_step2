package com.msquaredynamics.exercise03

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_titles.view.*



class TitlesFragment : Fragment(), View.OnClickListener {

    private var listener: OnArticleSelectedListener? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_titles, container, false)
        view.textview_frag_titles_title01.setOnClickListener(this)
        view.textview_frag_titles_title02.setOnClickListener(this)
        view.textview_frag_titles_title03.setOnClickListener(this)
        return view
    }


    override fun onClick(mView: View?) {
        when(mView!!.id) {
            R.id.textview_frag_titles_title01 -> listener?.onArticleSelected(articles[0].id)

            R.id.textview_frag_titles_title02 -> listener?.onArticleSelected(articles[1].id)

            R.id.textview_frag_titles_title03 -> listener?.onArticleSelected(articles[2].id)
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnArticleSelectedListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnArticleSelectedListener")
        }
    }


    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    interface OnArticleSelectedListener {
        fun onArticleSelected(id: Int)
    }
}
